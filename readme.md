## The QuAD
**Qu**ality **A**ssignment **D**isplay

The QuAD is a PHP/Javascript-driven webtool meant to enable display of quality work for students in a classroom setting.

By default, the QuAD plays random 20-second chunks of all videos in its "videos" subfolder.

## Installation

It has been tested on an Ubuntu webserver, but it should work on any web server running PHP and Javascript.

1. Simply clone it to any directory accessible on your webserver:
    * `$ git clone https://bitbucket.org/App_IT/theQuAD`
2. Add your videos (in mp4 format) to:
    * `/path/to/theQuAD/videos`
3. Point a browser to:
    * `<your webserver ip>/path/to/theQuAD`

## Customization

You can change the "refresh time" (how long a video plays before the next video starts) and "video directory" at the top of index.php

You may also be able to change the supported file type option, but it may or may not work depending on the file type and your system

## License

Open Source / MIT License