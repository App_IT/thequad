<html>
    <?php
    // SETTINGS and CONFIG
    
    $videoDirectory = './videos'; // video directory (relative path!)
    
    $fileType = 'mp4'; // what kind of videos are we searching for (extensions)?
    
    $refreshTime = 20; // how many seconds to show each video?
    
    ?>
    
    <head><title>The App Academy Quality Assignment Display!</title></head>
    
    <style>
        .centered {
            margin-right: auto;
            margin-left: auto;
            text-align: center;
        }
        
        .wrapper {
            margin-top: 30px;  
            width: 99%;
        }
        
        h1, h2, h3, h4, h5 {
            margin: 10px;
        }

    </style>
    
    <body>
    <div class = "wrapper centered">
        <div class = "centered">
            <?php
            // using a session variable to keep track of the video index
            // which decides which video to play
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            
            // getting a new array of video files each time the page is loaded
            // will ensure videos can be added on the fly
            foreach (glob($videoDirectory . '/*.' . $fileType) as $file) {
                $videos[] = $file;
            }
            
            // stop everything if there are no videos in the directory
            if (!$videos) {
                die(
                '<br /><h2>No videos in directory ' . $videoDirectory . '</h2>
                <h2><a href="javascript:window.location.reload();">Click here after adding videos</a></h2>'
                );
            }
            
            //  count the videos and set the total as a limit for the index counter, also offest by one because php arrays atart at 0
            $limit = (count($videos) - 1);
            
            // start an index at 0, increment it with every refresh, and reset it when the last video has played
            if (!isset($_SESSION['current_index']) || $_SESSION['current_index'] >= $limit) {
                $_SESSION['current_index'] = 0;
            } else {
                $_SESSION['current_index']++;
            }
            
            // extract the currently indexed video from the video array
            $video = $videos[$_SESSION['current_index']];

            // this regex pattern finds a filename by matching text between the last '-' and a dot + 3 or 4-letter file extension
            preg_match("/(?<=\]-).*(?=\.\w{3,4}$)/", $video, $name_match);
            $videoName = $name_match[0];
    
            // try to get the author of the app, if known
            preg_match("/(?<=\[).*(?=\]-)/", $video, $author_match);
            if ($author_match) {
                $author = $author_match[0];
            } else {
                $author = NULL;
            }
            
            // display the name of the video, given by the filename
            echo '<img class = "centered" src="logo.png" />';
            echo '<h2 class = "centered">"' . $videoName . '"</h2>';
            
            // credit the author, if given in the filename
            if ($author) {
                echo '<h4>by ' . $author . '</h4>';
            }
            
            // play the currently indexed video, Javascript will alter the video's start time below
            echo '<video id="video1" width="1280" height="720" src="' . $video . '" autoplay >Your browser doesn\'t support this HTML5 video tag.</video>';
            
            // make a footer to show some useful information for debuggin
            // the actual information is inserted by Javascript below
            echo '<p id="footer" style="color: LightGrey"></p>';
            
            ?>
            
            <script>
                    // make a function for easy page refreshing
                    function refreshAfter(seconds) {
                        setTimeout("location.reload(true);", (seconds * 1000));
                    }
                
                    // set a variable for easier access to info about the video
                    var js_video = document.getElementById("video1");
                    
                    // wait (listen) for meta-data to be loaded, then start controlling the video
                    js_video.addEventListener('loadedmetadata', function() {

                        // get $refreshTime from php so Javascript can use it
                        var js_refreshTime = <?php echo $refreshTime ?>;
                        
                        // get the video length (duration) in seconds, and round it down to the nearest integer
                        var js_videoLength = Math.floor(js_video.duration);
                        
                        if (js_videoLength > js_refreshTime) {
                            // find a random time to start the video between its beginning and $refreshTime before its end
                            var js_startTime = Math.floor((Math.random() * (js_videoLength - js_refreshTime)) + 1);
                            
                            // refresh the page as defined by $refreshTime at the top config
                            refreshAfter(js_refreshTime);
                            
                        } else {
                            // start at the beginning of the video
                            var js_startTime = 1;
                            
                            // refresh the page 1 second before the video ends
                            refreshAfter((js_videoLength - 1));
                        }
                        
                        // adjust the start time of the video according to the js_startTime variable set above
                        js_video.currentTime = js_startTime;
                        
                        // put some info in the footer for debugging (S = Video Start Time, L = Video Length)
                        document.getElementById('footer').innerHTML = '[S: ' + js_startTime + '][L: ' + js_videoLength + ']';    
                    });          
            </script>
            
        </div>
    </div>
    </body>
</html>